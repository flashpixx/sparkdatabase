package com.mindmanufacture.sparkdatabase.grpcapi;

import io.micronaut.context.annotation.Property;
import jakarta.inject.Singleton;

import javax.annotation.Nonnull;


@Singleton
public class GlobalConfig
{
    @Property( name = "k8.object.prefix" )
    private String m_k8ObjectPrefix;

    @Nonnull
    public String k8NamePrefix()
    {
        return m_k8ObjectPrefix;
    }
}
