package com.mindmanufacture.sparkdatabase.grpcapi;

import com.google.protobuf.Empty;
import com.mindmanufacture.sparkdatabase.api.v1.SparkServiceGrpc;
import com.mindmanufacture.sparkdatabase.api.v1.SqlRequest;
import io.fabric8.kubernetes.api.model.ObjectMeta;
import io.fabric8.kubernetes.client.DefaultKubernetesClient;
import io.fabric8.kubernetes.client.KubernetesClient;
import io.fabric8.kubernetes.client.utils.KubernetesResourceUtil;
import io.grpc.stub.StreamObserver;
import jakarta.inject.Inject;
import jakarta.inject.Singleton;
import tech.stackable.spark.v1alpha1.Driver;
import tech.stackable.spark.v1alpha1.Executor;
import tech.stackable.spark.v1alpha1.SparkApplication;
import tech.stackable.spark.v1alpha1.SparkApplicationSpec;
import tech.stackable.spark.v1alpha1.SparkImagePullSecrets;

import javax.annotation.Nonnull;
import java.nio.ByteBuffer;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.MessageFormat;
import java.util.Base64;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.UUID;
import java.util.stream.Collectors;
import java.util.stream.Stream;


/**
 * grpc endpoint for spark execution
 * <a href="https://github.com/stackabletech/spark-k8s-operator/blob/main/deploy/crd/sparkapplication.crd.yaml">crd</a>
 * <a href="https://github.com/fabric8io/kubernetes-client/tree/master/java-generator">code generator for pojos</a>
 */
@Singleton
public class SparkSQLEndpoint extends SparkServiceGrpc.SparkServiceImplBase
{
    private static final int HASHSIZE = 6;
    private static final String KUBEOBJECTFORMAT = "{0}-{1}-{2}";
    private static final String MEMORYUNIT = "M";
    private static final String CPUUNIT = "m";
    @Inject
    private GlobalConfig m_globalConfig;
    @Inject
    private SparkConfig m_sparkConfig;

    @Override
    public void query( @Nonnull final SqlRequest p_request, @Nonnull final StreamObserver<Empty> p_response )
    {
        final ObjectMeta l_meta = new ObjectMeta();
        l_meta.setName(
            KubernetesResourceUtil.sanitizeName(
                MessageFormat.format(
                    KUBEOBJECTFORMAT,
                    m_globalConfig.k8NamePrefix(),
                    m_sparkConfig.podName(),
                    uuid2hash( UUID.randomUUID() )
                )
            )
        );

        final SparkApplication l_job = new SparkApplication();
        l_job.setMetadata( l_meta );
        l_job.setSpec( this.buildSpec( p_request ) );

        try
        (
            final KubernetesClient l_client = new DefaultKubernetesClient()
        )
        {
            l_client.resources( SparkApplication.class )
                    .inNamespace( l_client.getNamespace() )
                    .create( l_job );
        }
        catch ( final Exception l_exception )
        {
            l_exception.printStackTrace();
            throw new RuntimeException( l_exception );
        }

        p_response.onNext( Empty.newBuilder().build() );
        p_response.onCompleted();
    }

    @Nonnull
    private SparkApplicationSpec buildSpec( @Nonnull final SqlRequest p_request )
    {
        final SparkApplicationSpec l_spec = new SparkApplicationSpec();
        final Map<String, String> l_sparkConf = new HashMap<>();

        l_spec.setVersion( "1.0" );
        l_spec.setMode( "cluster" );
        l_spec.setSparkImage( m_sparkConfig.image() );
        l_spec.setMainApplicationFile( m_sparkConfig.mainApplicationFile() );
        l_spec.setMainClass( m_sparkConfig.mainClass() );
        l_spec.setSparkConf( l_sparkConf );
        l_spec.setArgs( Stream.of( Base64.getEncoder().encodeToString( p_request.toByteArray() ) ).collect( Collectors.toList() ) );

        p_request.getSpark()
                 .getNodeSelectorMap()
                 .forEach( ( key, value ) -> l_sparkConf.put( MessageFormat.format( "spark.kubernetes.node.selector.{0}", key ), value ) );

        l_sparkConf.putAll( m_sparkConfig.sparkConf() );

        if ( Objects.nonNull( m_sparkConfig.imagePullSecrets() ) && !m_sparkConfig.imagePullSecrets().isEmpty() )
        {
            final SparkImagePullSecrets l_secret = new SparkImagePullSecrets();
            l_secret.setName( m_sparkConfig.imagePullSecrets() );
            l_spec.setSparkImagePullSecrets( Stream.of( l_secret ).collect( Collectors.toList() ) );
        }

        final Driver l_driver = new Driver();
        l_spec.setDriver( l_driver );
        l_driver.setMemory( MessageFormat.format( "{1}{0}", MEMORYUNIT, Integer.toString( p_request.getSpark().getDriver().getMemoryMegabyte() ) ) );
        l_driver.setCoreLimit( MessageFormat.format( "{1}{0}", CPUUNIT, Integer.toString( p_request.getSpark().getDriver().getCoreLimitMilli() ) ) );
        l_driver.setCores( Long.valueOf( p_request.getSpark().getDriver().getCores() ) );

        final Executor l_executor = new Executor();
        l_spec.setExecutor( l_executor );
        l_executor.setMemory( MessageFormat.format( "{1}{0}", MEMORYUNIT, Integer.toString( p_request.getSpark().getExecutor().getMemoryMegabyte() ) ) );
        l_executor.setCores( Long.valueOf( p_request.getSpark().getExecutor().getCores() ) );
        l_executor.setInstances( Long.valueOf( p_request.getSpark().getExecutor().getInstances() ) );

        return l_spec;
    }

    @Nonnull
    private static String uuid2hash( @Nonnull final UUID p_uuid )
    {
        final ByteBuffer l_buffer = ByteBuffer.wrap( new byte[16] );
        l_buffer.putLong( p_uuid.getMostSignificantBits() );
        l_buffer.putLong( p_uuid.getLeastSignificantBits() );

        final byte[] l_bytes;
        try
        {
            final MessageDigest l_hash = MessageDigest.getInstance( "SHA-1" );
            l_hash.update( l_buffer );
            l_bytes = l_hash.digest();
        }
        catch ( final NoSuchAlgorithmException l_exception )
        {
            throw new RuntimeException( l_exception );
        }

        final StringBuilder l_hex = new StringBuilder();
        for ( int i = 0; i < l_bytes.length && i < HASHSIZE; i++ )
            l_hex.append( String.format( "%02x", l_bytes[i] ) );

        return l_hex.toString();
    }
}
