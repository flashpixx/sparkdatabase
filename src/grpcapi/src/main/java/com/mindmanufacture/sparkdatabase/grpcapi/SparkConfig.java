package com.mindmanufacture.sparkdatabase.grpcapi;

import io.micronaut.context.annotation.ConfigurationProperties;
import io.micronaut.core.convert.format.MapFormat;
import io.micronaut.core.naming.conventions.StringConvention;
import jakarta.inject.Singleton;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.Collections;
import java.util.Map;


@Singleton
@ConfigurationProperties( "spark" )
public class SparkConfig
{
    private Map<String, String> m_sparkConf = Collections.emptyMap();
    private String m_image;
    private String m_mainApplicationFile;
    private String m_mainClass;
    private String m_pullSecrets;
    private String m_podName;

    public void setConf( @MapFormat( transformation = MapFormat.MapTransformation.FLAT, keyFormat = StringConvention.RAW )
                         @Nonnull final Map<String, String> p_conf )
    {
        m_sparkConf = p_conf;
    }

    @Nonnull
    public String image()
    {
        return m_image;
    }

    @Nonnull
    public String mainApplicationFile()
    {
        return m_mainApplicationFile;
    }

    @Nonnull
    public String mainClass()
    {
        return m_mainClass;
    }

    @Nullable
    public String imagePullSecrets()
    {
        return m_pullSecrets;
    }

    @Nonnull
    public String podName()
    {
        return m_podName;
    }

    public void setImage( @Nonnull final String p_image )
    {
        m_image = p_image;
    }

    public void setMainApplicationFile( @Nonnull final String p_file )
    {
        m_mainApplicationFile = p_file;
    }

    public void setMainClass( @Nonnull final String p_mainClass )
    {
        m_mainClass = p_mainClass;
    }

    public void setPullSecrets( @Nonnull final String p_pullSecrets )
    {
        m_pullSecrets = p_pullSecrets;
    }

    public void setPodName( @Nonnull final String p_podName )
    {
        m_podName = p_podName;
    }

    @Nonnull
    public Map<String, String> sparkConf()
    {
        return Collections.unmodifiableMap( m_sparkConf );
    }
}
