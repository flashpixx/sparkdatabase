package com.mindmanufacture.sparkdatabase.grpcapi;

import io.micronaut.runtime.Micronaut;

import javax.annotation.Nonnull;


//Checkstyle:OFF:HideUtilityClassConstructor
public final class Application
{
    /**
     * main
     *
     * @param p_args arguments
     */
    //Checkstyle:OFF:UncommentedMain
    public static void main( @Nonnull final String[] p_args )
    {
        Micronaut.run( Application.class, p_args );
    }
    //Checkstyle:ON:UncommentedMain

}
//Checkstyle:ON:HideUtilityClassConstructor
