package main

import (
	"context"
	_ "embed"
	"flag"
	gw "gateway/service"
	"github.com/grpc-ecosystem/grpc-gateway/v2/runtime"
	v3 "github.com/swaggest/swgui/v3"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials/insecure"
	"k8s.io/klog"
	"net/http"
	"os"
	"strconv"
	"strings"
)

const (
	endpointFlag      = "endpoint"
	addressFlag       = "address"
	swaggerURLFlag    = "swaggerui-url"
	swaggerEnableFlag = "swaggerui-enable"
)

var (
	//go:embed service/v1-sparksql.swagger.json
	swaggerJson []byte

	endpoint      = flag.String(endpointFlag, "localhost:50051", "gRPC server endpoint")
	address       = flag.String(addressFlag, ":8081", "bind address wih port")
	swaggerURL    = flag.String(swaggerURLFlag, "/docs", "swagger-ui url prefix")
	swaggerEnable = flag.Bool(swaggerEnableFlag, true, "enable swagger-ui")
)

func restHandler(root *http.ServeMux) error {
	ctx := context.Background()
	ctx, cancel := context.WithCancel(ctx)
	defer cancel()

	mux := runtime.NewServeMux()
	opts := []grpc.DialOption{grpc.WithTransportCredentials(insecure.NewCredentials())}
	if err := gw.RegisterSparkServiceHandlerFromEndpoint(ctx, mux, *endpoint, opts); err != nil {
		return err
	}

	root.Handle("/", mux)
	return nil
}

func swaggerHandler(root *http.ServeMux) {
	swaggerJsonURL := *swaggerURL + "/swagger.json"

	root.Handle(*swaggerURL, v3.NewHandler("gRPC Gateway", swaggerJsonURL, *swaggerURL))
	root.HandleFunc(swaggerJsonURL, func(w http.ResponseWriter, r *http.Request) {
		w.WriteHeader(http.StatusOK)
		w.Header().Set("Content-Type", "application/json")
		_, err := w.Write(swaggerJson)

		if err != nil {
			klog.Error(err)
		}
	})
}

func main() {
	klog.InitFlags(nil)
	flag.Parse()
	defer klog.Flush()

	if val, ok := os.LookupEnv(strings.ToUpper(endpointFlag)); ok {
		endpoint = &val
	}
	if val, ok := os.LookupEnv(strings.ToUpper(addressFlag)); ok {
		address = &val
	}
	if val, ok := os.LookupEnv(strings.ToUpper(swaggerURLFlag)); ok {
		swaggerURL = &val
	}
	if val, ok := os.LookupEnv(strings.ToUpper(swaggerEnableFlag)); ok {
		b, _ := strconv.ParseBool(val)
		swaggerEnable = &b
	}

	mux := http.NewServeMux()

	if *swaggerEnable {
		klog.Infoln("enable swagger-ui under", *swaggerURL)
		swaggerHandler(mux)
	}

	klog.Infoln("grpc endpoint is set to", *endpoint)
	if err := restHandler(mux); err != nil {
		klog.Fatal(err)
	}

	klog.Infoln("bind server on", *address)
	if err := http.ListenAndServe(*address, mux); err != nil {
		klog.Fatal(err)
	}

	klog.Flush()
}
