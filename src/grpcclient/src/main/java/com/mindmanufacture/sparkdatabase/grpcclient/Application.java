package com.mindmanufacture.sparkdatabase.grpcclient;

import com.mindmanufacture.sparkdatabase.api.v1.DatabaseSettings;
import com.mindmanufacture.sparkdatabase.api.v1.SparkDriver;
import com.mindmanufacture.sparkdatabase.api.v1.SparkExecutor;
import com.mindmanufacture.sparkdatabase.api.v1.SparkServiceGrpc;
import com.mindmanufacture.sparkdatabase.api.v1.SparkSettings;
import com.mindmanufacture.sparkdatabase.api.v1.SqlRequest;
import io.grpc.netty.shaded.io.grpc.netty.GrpcSslContexts;
import io.grpc.netty.shaded.io.grpc.netty.NettyChannelBuilder;
import io.grpc.netty.shaded.io.netty.handler.ssl.util.InsecureTrustManagerFactory;

import javax.annotation.Nonnull;
import javax.net.ssl.SSLException;
import java.util.Collections;


//Checkstyle:OFF:HideUtilityClassConstructor
public final class Application
{
    /**
     * main
     *
     * @param p_args arguments
     */
    //Checkstyle:OFF:UncommentedMain
    public static void main( @Nonnull final String[] p_args ) throws SSLException
    {
        final SparkServiceGrpc.SparkServiceBlockingStub l_stub = SparkServiceGrpc.newBlockingStub(
            NettyChannelBuilder.forAddress( p_args[0], Integer.parseInt( p_args[1] ) )
                               .sslContext( GrpcSslContexts.forClient().trustManager( InsecureTrustManagerFactory.INSTANCE ).build() )
                               .build()
        );

        l_stub.query(
            SqlRequest.newBuilder()
                      .setSparkQuery( "SELECT * FROM foo" )
                      .setDatabase(
                          DatabaseSettings.newBuilder()
                                          .setJdbc( "jdbc:postgresql://sk-postgresql-hl/postgres" )
                                          .setUsername( p_args[2] )
                                          .setPassword( p_args[3] )
                                          .addSourceTable( "foo" )
                                          .setTargetTable( "bar" )
                                          .build()
                      )
                      .setSpark(
                          SparkSettings.newBuilder()
                                       .putAllNodeSelector( Collections.emptyMap() )
                                       .setDriver(
                                           SparkDriver.newBuilder()
                                                      .setCores( 1 )
                                                      .setCoreLimitMilli( 1200 )
                                                      .setMemoryMegabyte( 500 )
                                                      .build()
                                       )
                                       .setExecutor(
                                           SparkExecutor.newBuilder()
                                                        .setCores( 1 )
                                                        .setInstances( Integer.parseInt( p_args[4] ) )
                                                        .setMemoryMegabyte( 500 )
                                                        .build()
                                       )
                                       .build()
                      )
                      .build()
        );
    }
    //Checkstyle:ON:UncommentedMain

}
//Checkstyle:ON:HideUtilityClassConstructor
