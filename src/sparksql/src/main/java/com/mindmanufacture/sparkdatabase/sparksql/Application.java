package com.mindmanufacture.sparkdatabase.sparksql;

import com.mindmanufacture.sparkdatabase.api.v1.SqlRequest;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.SaveMode;
import org.apache.spark.sql.SparkSession;

import javax.annotation.Nonnull;
import java.util.Base64;


//Checkstyle:OFF:HideUtilityClassConstructor
public final class Application
{
    private static final String APP = "sql";
    private static final String FORMAT = "jdbc";
    private static final String URL = "url";
    private static final String USER = "user";
    private static final String PASSWORD = "password";
    private static final String TABLE = "dbtable";

    /**
     * main
     *
     * @param p_args arguments
     */
    //Checkstyle:OFF:UncommentedMain
    public static void main( @Nonnull final String[] p_args ) throws Exception
    {
        if ( p_args.length < 1 )
            throw new RuntimeException( "not enough arguments for execution" );

        final SqlRequest l_request = SqlRequest.parseFrom( Base64.getDecoder().decode( p_args[0] ) );

        try
        (
            final SparkSession l_session = SparkSession.builder().appName( APP ).getOrCreate()
        )
        {
            l_request.getDatabase()
                     .getSourceTableList()
                     .forEach( i -> l_session.read()
                                             .format( FORMAT )
                                             .option( URL, l_request.getDatabase().getJdbc() )
                                             .option( USER, l_request.getDatabase().getUsername() )
                                             .option( PASSWORD, l_request.getDatabase().getPassword() )
                                             .option( TABLE, i )
                                             .load()
                                             .createOrReplaceTempView( i ) );

            final Dataset<?> l_result = l_session.sql( l_request.getSparkQuery() );
            l_result.show( 20, false );

            l_result
                  .write()
                  .mode( SaveMode.Append )
                  .format( FORMAT )
                  .option( URL, l_request.getDatabase().getJdbc() )
                  .option( USER, l_request.getDatabase().getUsername() )
                  .option( PASSWORD, l_request.getDatabase().getPassword() )
                  .option( TABLE, l_request.getDatabase().getTargetTable() )
                  .save();

        }
        catch ( final Exception l_exception )
        {
            l_exception.printStackTrace();
            throw l_exception;
        }
    }
    //Checkstyle:ON:UncommentedMain
}
//Checkstyle:ON:HideUtilityClassConstructor
