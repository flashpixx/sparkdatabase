.PHONY: install cluster clean build run

PROJECT=sparkdatabase
HELMDIR=$(CURDIR)/deploy/helm
TERRAFORMDIR=$(CURDIR)/deploy/terraform
KUBECONFIG=sparkdatabase-kubeconfig.yml

MVNGOAL=deploy
MVNARGS=-B -Ddocker.insecure=true -DsendCredentialsOverHttp=true -Ddocker.user=admin -Ddocker.password=admin123 -Ddocker.prefix=localhost:8083/$(PROJECT)/ -Ddocker.tag=latest
MVNBUILDAPI=-Dpackaging=docker-native -Pgraalvm -am -pl src/grpcapi $(MVNGOAL)
MVNBUILDGATEWAYBUILD=-am -pl src/gatewaybuild $(MVNGOAL)
MVNBUILDGATEWAY=-Pdocker -am -pl src/api $(MVNGOAL)
MVNBUILDSPARKSQL=-am -pl src/sparksql $(MVNGOAL)

TFARGS=-var="kube_config_path=../../$(KUBECONFIG)" -var="app_maven_arguments=$(MVNARGS)" -var='app_maven_commands=["$(MVNBUILDGATEWAYBUILD)", "$(MVNBUILDGATEWAY)", "$(MVNBUILDAPI)", "$(MVNBUILDSPARKSQL)"]'


# installs the whole environment
install: grpcclient
	@terraform -chdir=$(TERRAFORMDIR) init -upgrade
	@terraform -chdir=$(TERRAFORMDIR) apply -auto-approve $(TFARGS)

# destroys and reinstalls the cluster with application
cluster:
	@terraform -chdir=$(TERRAFORMDIR) destroy -auto-approve -target kind_cluster.k8s $(TFARGS)
	@terraform -chdir=$(TERRAFORMDIR) apply -auto-approve $(TFARGS)

# destroy the whole environment
clean:
	@terraform -chdir=$(TERRAFORMDIR) destroy -auto-approve -var="kube_config_path=../../$(KUBECONFIG)" -var="app_maven_arguments=$(MVNARGS)" -var='app_maven_commands=["$(MVNBUILDAPI)", "$(MVNBUILDSPARKSQL)"]' || true
	@./internal/dockerclean.sh $(PROJECT) || true
	@./internal/dockerclean.sh gatewaybuild || true
	@./internal/dockerclean.sh sparkparent || true
	@./internal/dockerclean.sh dockerparent || true
	@rm -f $(TERRAFORMDIR)/terraform.tfstate*
	@rm -Rf $(TERRAFORMDIR)/.terraform*
	@rm -f $(HELMDIR)/Chart.lock
	@rm -Rf $(HELMDIR)/charts
	@rm -f $(CURDIR)/$(KUBECONFIG)
	@rm -Rf $(CURDIR)/.maven
	@mvn clean


# builds all applications
build: gatewaybuild gateway grpcapi sparksql grpcclient

# build gatewaybuild container
gatewaybuild:
	@mvn $(MVNARGS) $(MVNBUILDGATEWAYBUILD)

# build gateway container
gateway:
	@mvn $(MVNARGS) $(MVNBUILDGATEWAY)

# builds the api container
grpcapi:
	@mvn $(MVNARGS) $(MVNBUILDAPI)

# builds the spark container
sparksql:
	@mvn $(MVNARGS) $(MVNBUILDSPARKSQL)

# builds the cli client
grpcclient:
	@mvn -B -am -pl src/grpcclient package

# runs the cli client
run:
	@java -jar src/grpcclient/target/grpcclient.jar sparkgrpc.local 443 postgres admin123 1
