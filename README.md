# Spark Database

gRPC Micronaut service to query a Postgres database with Spark operator

## API

* GRPC server on `sparkgrpc.local:443`
* GRPC REST Gateway [http://sparkrest.local](http://sparkrest.local) (Swagger-UI [http://sparkrest.local/docs](http://sparkrest.local/docs))
* Postgres (credentials: `postgres`:`admin123`) bind on `localhost:30000`


## Setup Development Environment

Requirements (executable within the `PATH` or installed):

* Make
* Docker
* Maven 3.3.9+ and JDK 11
* [Kubernetes KinD](https://kind.sigs.k8s.io/)
* [Terraform](https://www.terraform.io/)
* [Kubectl](https://kubernetes.io/docs/tasks/tools/)
* [Helm](https://helm.sh/)

Run `make install` for setting up the whole environment and `make clean` for clear everything. See the [Makefile documentation](Makefile) for other command
targets. For test execution run the command `make run`.

### Localhost Network Configuration

Append the following lines into your `/etc/hosts` (*nix systems) / `C:\Windows\System32\Drivers\etc\hosts` (Windows system) file (root / admin access required):

```
127.0.0.1 sparkgrpc.local
```

## Architecture

### Build

```mermaid
flowchart LR
  subgraph Maven-Build
    A[API] -->|package| B(GraalVM Java API Container)
    C[SparkSQL] -->|package| D(Redhat UBI Spark Container)
  end  
  subgraph Local-Nexus
    B -->|deploy| G(Docker Registry)
    D -->|deploy| G
  end
  H[Docker Hub Container] -->|pull| G
  subgraph Local-Kubernetes
    G -->|pull| I(Pods)
    I --> J(Services)
    J --> K[Ingress]
  end
  T[Terraform] -.->|apply| Maven-Build
  T -.->|apply| Local-Nexus
  T -.->|apply| Local-Kubernetes
  T -.->|apply| M[Helm Chart]
  M -->|install| I
  M -->|install| J
  M -->|install| K
```

### Runtime Execution

#### Functionality

```mermaid
sequenceDiagram
  gRPC Client->>+gRPC Server: execute Protobuf definition<br/>via gRPC Request
  gRPC Server->>+K8 API: execute Custom Resource Definition (CRD)<br/>via REST Request
  gRPC Server->>-gRPC Client: gRPC Response<br/>with Protobuf definition
  K8 API->>+K8 Spark Operator: triggers
  K8 Spark Operator->>+K8 Spark Cluster: generate Pods
  K8 Spark Cluster->>+Spark Application: execute Spark Submit
  Spark Application->>-K8 Spark Cluster: close Spark Session
  K8 Spark Cluster->>-K8 Spark Operator: terminates Pods
```

#### Data Transfer

```mermaid
sequenceDiagram
    gRPC Client->>+K8 Ingress: gRPC HTTPS 2 Request
    K8 Ingress->>+K8 Service: pass gRPC HTTP 2 Request
    K8 Service->>+K8 API Pod: receive gRPC HTTP 2 Request
    K8 API Pod->>+K8 Spark Operator: spawns SparkApplication Job <br/>by passing binary gRPC Request
    K8 API Pod->>-K8 Service: send gRPC response
    K8 Service->>-K8 Ingress: pass gRPC response
    K8 Ingress->>-gRPC Client: receive gRPC response
    K8 Spark Operator->>+Spark Driver: spawns Spark Driver & Executor Pods <br/>and pass gRPC Request
    Spark Driver->>+Spark Executor 1..N: open Spark Sesssion and <br/> pass executor information
    Spark Executor 1..N->>+Postgres: jdbc connect to tables
    Postgres->>+Spark Executor 1..N: temporary views
    Note right of Spark Executor 1..N: execute SQL statement <br/> for data subset 1..N
    Spark Executor 1..N->>+Postgres: insert results in table
    Spark Executor 1..N->>-Spark Driver: close Spark Session
    Spark Driver->>+K8 Spark Operator: terminate Spark Pods
```


### Dependencies

![dep](https://gitlab.com/flashpixx/sparkdatabase/-/jobs/artifacts/main/raw/dependency-graph.png?job=dependency-graph-deploy)