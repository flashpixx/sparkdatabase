#!/bin/bash -e

NAME=$1
docker rmi -f $(docker images --format '{{.Repository}}:{{.Tag}}' | grep $NAME)