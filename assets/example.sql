create table if not exists foo
(
    id serial,
    value integer not null,
    constraint foo_pk
    primary key (id)
);

create unique index if not exists foo_id_uindex
    on foo (id);

create table if not exists bar
(
    id integer not null,
    value integer not null,
    constraint bar_pk
    primary key (id)
);

create unique index if not exists bar_id_uindex
    on bar (id);

insert into foo (value) values (3);
insert into foo (value) values (5);
insert into foo (value) values (7);
insert into foo (value) values (8);
insert into foo (value) values (51);
