terraform {
  required_version = ">= 0.13"

  required_providers {
    kind = {
      source  = "tehcyx/kind"
      version = "0.0.13"
    }

    docker = {
      source  = "kreuzwerker/docker"
      version = "2.16.0"
    }

    http = {
      source  = "hashicorp/http"
      version = "2.2.0"
    }

    #kubectl = {
    #  source  = "gavinbunney/kubectl"
    #  version = "1.14.0"
    #}

    helm = {
      version = "~> 2.4.1"
    }

    tls = {
      source  = "hashicorp/tls"
      version = "3.4.0"
    }
  }
}

provider "tls" {}

provider "docker" {}

provider "http" {}

provider "kind" {}

#provider "kubectl" {
#  apply_retry_count      = 10
#  load_config_file       = false
#  host                   = kind_cluster.k8s.endpoint
#  client_certificate     = base64encode(kind_cluster.k8s.client_certificate)
#  cluster_ca_certificate = base64encode(kind_cluster.k8s.cluster_ca_certificate)
#}

provider "helm" {
  kubernetes {
    config_path = local.kubeConfigPath
  }
}