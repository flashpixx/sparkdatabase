locals {
  appSeparator = "-"
}

resource "docker_network" "k8s" {
  name = "kind"
}

resource "docker_image" "registry" {
  name = join(local.appSeparator, [var.name, "registry"])
  build {
    path       = var.registry_build_context
    dockerfile = var.registry_dockerfile
  }
}

resource "docker_container" "registry" {
  name  = join(local.appSeparator, [var.name, "registry"])
  image = docker_image.registry.latest

  networks_advanced {
    name = docker_network.k8s.name
  }

  dynamic "ports" {
    for_each = var.registry_ports

    content {
      internal = ports.value
      external = ports.value
    }
  }
}

resource "null_resource" "registry-wait" {
  depends_on = [docker_container.registry]

  provisioner "local-exec" {
    command = join("", ["until curl -k -s http://localhost:", tostring(var.registry_ui_port), " >/dev/null; do sleep 5; done"])
  }
}