locals {
  kubeConfigPath = var.kube_config_path == "" ? join("/", [path.module, join("-", [var.name, "kubeconfig.yml"])]) : var.kube_config_path
}


resource "kind_cluster" "k8s" {
  depends_on      = [docker_network.k8s]
  name            = var.name
  node_image      = var.cluster_image
  wait_for_ready  = true
  kubeconfig_path = local.kubeConfigPath

  kind_config {
    kind        = "Cluster"
    api_version = "kind.x-k8s.io/v1alpha4"

    containerd_config_patches = [
      join("", [
        "[plugins.\"io.containerd.grpc.v1.cri\".registry.mirrors.\"docker.io\"]\n  endpoint = [\"http://", docker_container.registry.name, ":",
        tostring(var.registry_pull_port), "\"]"
      ])
    ]

    networking {
        api_server_address = var.cluster_bind_address
        api_server_port = var.cluster_bind_port
    }

    node {
      role = "control-plane"

      kubeadm_config_patches = [
        yamlencode({
          "kind" : "InitConfiguration",
          "nodeRegistration" : {
            "kubeletExtraArgs" : {
              "node-labels" : "ingress-ready=true"
            }
          }
        })
      ]


      dynamic "extra_port_mappings" {
        for_each = var.cluster_node_ports

        content {
          container_port = extra_port_mappings.value
          host_port      = extra_port_mappings.value
        }
      }
    }

    dynamic "node" {
      for_each = range(var.cluster_nodes)

      content {
        role = "worker"
      }
    }
  }
}

resource "null_resource" "ingress" {
  depends_on = [kind_cluster.k8s]

  provisioner "local-exec" {
    command = join(" ", [
      "kubectl", "--kubeconfig", local.kubeConfigPath, "apply", "-f",
      "https://raw.githubusercontent.com/kubernetes/ingress-nginx/main/deploy/static/provider/kind/deploy.yaml"
    ])
  }
}

resource "null_resource" "ingress-wait" {
  depends_on = [null_resource.ingress]

  provisioner "local-exec" {
    command = join(" ", [
      "kubectl", "--kubeconfig", local.kubeConfigPath, "wait", "--namespace ingress-nginx", "--for=condition=ready", "pod",
      "--selector=app.kubernetes.io/component=controller", "--timeout=180s"
    ])
  }
}

#data "http" "ingress" {
#  url = "https://raw.githubusercontent.com/kubernetes/ingress-nginx/main/deploy/static/provider/kind/deploy.yaml"
#}

#data "kubectl_file_documents" "ingress" {
#  content = data.http.ingress.response_body
#}

#resource "kubectl_manifest" "ingress" {
#  for_each         = data.kubectl_file_documents.ingress.manifests
#  yaml_body        = each.value
#  wait_for_rollout = true
#  force_new        = true
#}