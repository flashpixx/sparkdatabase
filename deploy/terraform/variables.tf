variable "name" {
  description = "name of the project and all dependency elements"
  type        = string
}

variable "cluster_bind_address" {
  description = "ip bind address of the cluster"
  type        = string
  default     = "127.0.0.1"
}

variable "cluster_bind_port" {
  description = "port of the cluster configuration"
  type        = number
  default     = 6500
}

variable "cluster_image" {
  description = "kind cluster image"
  type        = string
  default     = "kindest/node:v1.21.1"
}

variable "cluster_node_ports" {
  description = "list of port mappings"
  type        = set(number)
  default     = [80, 443]
}

variable "cluster_nodes" {
  description = "number of worker nodes"
  type        = number
  default     = 1
}

variable "registry_ui_port" {
  description = "ui port of the registry"
  type        = number
}

variable "registry_pull_port" {
  description = "pull port of the registry"
  type        = number
}

variable "registry_build_context" {
  description = "docker build context of registry"
  type        = string
}

variable "registry_dockerfile" {
  description = "dockerfile for build"
  type        = string
}

variable "registry_ports" {
  description = "port mappings of registry container"
  type        = set(number)
}

variable "kube_config_path" {
  description = "path to generated kubeconfig file"
  type        = string
  default     = ""
}

variable "app_helm_chart" {
  description = "path of the helm chart of the application"
  type        = string
}

variable "app_helm_name" {
  description = "helm deployment name"
  type        = string
}

variable "app_tls_valid_hours" {
  description = "hours of validation of tls/ssl certification"
  type        = number
  default     = 720
}

variable "app_tls_regenerate_hours" {
  description = "hours of regeneration of tls/ssl before expire"
  type        = number
  default     = 24
}

variable "app_tls_dns" {
  description = "dns names of the tls/ssl"
  type        = list(string)
}

variable "app_maven_workdir" {
  description = "maven working directory with root pom.xml"
  type = string
}

variable "app_maven_arguments" {
  description = "maven general build arguments"
  type = string
  default = "-B"
}

variable "app_maven_commands" {
  description = "maven build commands"
  type = list(string)
}
