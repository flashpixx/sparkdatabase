resource "null_resource" "maven" {
  depends_on = [null_resource.registry-wait]
  for_each   = toset(var.app_maven_commands)

  provisioner "local-exec" {
    command     = join(" ", ["mvn", var.app_maven_arguments, each.value])
    working_dir = var.app_maven_workdir
  }
}

resource "tls_private_key" "ingress-api" {
  algorithm = "RSA"
  rsa_bits  = 4096
}

resource "tls_self_signed_cert" "ingress-api" {
  key_algorithm   = tls_private_key.ingress-api.algorithm
  private_key_pem = tls_private_key.ingress-api.private_key_pem

  validity_period_hours = var.app_tls_valid_hours
  early_renewal_hours   = var.app_tls_regenerate_hours

  dns_names    = var.app_tls_dns
  allowed_uses = [
    "key_encipherment",
    "digital_signature",
    "server_auth",
  ]

  subject {}
}

resource "helm_release" "app" {
  depends_on        = [kind_cluster.k8s, null_resource.maven, null_resource.ingress-wait]
  name              = var.app_helm_name
  chart             = var.app_helm_chart
  namespace         = var.name
  create_namespace  = true
  dependency_update = true
  recreate_pods     = true

  set {
    name  = "grpcapi.ingress.tls.cert"
    value = base64encode(tls_private_key.ingress-api.public_key_pem)
  }

  set {
    name  = "grpcapi.ingress.tls.key"
    value = base64encode(tls_private_key.ingress-api.private_key_pem)
  }
}